import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";

import { WebView } from "react-native-webview";

import * as Print from "expo-print";

export default class App extends Component {
  state = {
    loading: false,
  };
  goBack = () => {
    this.refs.myWebViewRef.goBack();
  };
  goForward = () => {
    this.refs.myWebViewRef.goForward();
  };

  handleWebViewMessage = (event) => {
    const data = JSON.parse(event.nativeEvent.data);
    if (data.action === "PRINT" && data.source === "myWebViewRef") {
      Print.printAsync({
        html: data.htmlToPrint,
      });
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header loading={this.state.loading} />
        <WebView
          source={{ uri: "http://192.168.31.190:3000" }}
          ref="myWebViewRef"
          onLoadStart={() => this.setState({ loading: true })}
          onLoadEnd={() => this.setState({ loading: false })}
          onMessage={(event) => this.handleWebViewMessage(event)}
        />
        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => this.goBack()}
            disabled={this.state.canGoBack}
          >
            <Text style={styles.icon}>&lt;</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.goForward()}>
            <Text style={styles.icon}>&gt;</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const Header = ({ loading }) => (
  <View style={styles.header}>
    <Text style={styles.title}>Mercury OS</Text>
    {loading ? <ActivityIndicator color="blue" /> : null}
  </View>
);

const styles = StyleSheet.create({
  header: {
    paddingTop: 20,
    paddingBottom: 10,
    justifyContent: "flex-start",
    backgroundColor: "#0281ac",
  },
  title: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
  icon: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
  footer: {
    padding: 2,
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: "#0281ac",
  },
});
